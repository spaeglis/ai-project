import cProfile
import pickle
import sys
import heapq
import json
import copy
import Board
import Piece
import Block


board = Board.Board()
exit_tiles = {'red':[(3,-3),(3,-2),(3,-1),(3,0)],
        'green':[(-3,3),(-2,3),(-1,3),(0,3)],
        'blue':[(0,-3),(-1,-2),(-2,-1),(-3,0)]}

tile_map = {"0-3":1,"1,-3":2,"2-3":3,"3-3":4,"-1-2":5,"0-2":6,"1-2":7,"2-2":8,
        "3-2":9,"-2-1":10,"-1-1":11,"0-1":12,"1-1":13,"2-1":14,"3-1":15,
        "-30":16,"-20":17,"-10":18,"00":19,"10":20,"20":21,"30":22,"-31":23,
        "-21":24,"-11":25,"01":26,"11":27,"21":28,"-32":29,"-22":30,"-12":31,
        "02":32,"12":33,"-33":34,"-23":35,"-13":36,"03":37}

def main():
    with open(sys.argv[1]) as file:
        
        # Load initial board state
        data = json.load(file)
        bdict = {}

        # Add pieces and blocks to the board
        for p in data['pieces']:
            piece = Piece.Piece(tuple(p), data['colour'])
            board.tiles[piece.pos[1]+3][piece.pos[0]+3] = piece
            board.pieces.append(piece)
        
        for b in data['blocks']:
            block = Block.Block(tuple(b))
            board.tiles[block.pos[1]+3][block.pos[0]+3] = block

        colour = data['colour']
        bdict = make_bdict(board)
        print_board(bdict)
         
        nonBlockTiles = []
        for tile in exit_tiles[colour]:
            if type(board.tiles[tile[1]+3][tile[0]+3]) == Block.Block:
                pass
            else:
                nonBlockTiles.append(tile)
        exit_tiles[colour] = nonBlockTiles
        board.exit_tiles[colour] = nonBlockTiles
        print(exit_tiles[colour])
        

        # Run a* search and print solution if one exists
        path = A_Star(board)
        if path is None:
            print("No path found")
        else:
            for node in path:
         
                if not node.toMove:
                    continue
                if node.toMove[0] == 'e':
                    print("EXIT from (" + str(node.toMove[1]) + ", " + str(node.toMove[2]) + ").")

                elif node.toMove[0] == 'm':
                    print("MOVE from " + str(node.toMove[1]) + " to " + str(node.toMove[2]) + ".")

                elif node.toMove[0] == 'j':
                    print ("JUMP from " + str(node.toMove[1]) + " to " + str(node.toMove[2]) + ".")


def makeHash(node):
    h = str(len(node.pieces))
    spos = []
    for p in node.pieces:
        s = str(p.pos[0]) + str(p.pos[1])
        spos.append(s)
    spos.sort()
    for piece in spos:
        h += piece
    return h




def A_Star(start):

    # Initialise open and closed sets
    closedSet = {}
    openSet = [start]
    openSet2 = {}
    idx = makeHash(start)
    openSet2[idx] = start
    
    # Initial path length and heuristic
    start.g = 0

    start.f = heuristic(start)
    
    while openSet:

        # Find node in open set with lowest f value and set it as the "current" node
        current = heapq.heappop(openSet)
        while current.duplicate == True:
            current = heapq.heappop(openSet)
        
        # If found node is the goal, retrace the path, and return the solution
        if checkGoal(current):
            return retrace(current) 

        # Remove the current node from the open set and add it to the closed set
        #openSet.remove(current)
        idx = makeHash(current)
        del openSet2[idx]
        closedSet[idx] = current
        
        #Find all neighbors of the current node
        neighbors = getNeighbors(current, closedSet)
        
        # Iterate through the neighbors of the current node
        for neighbor in neighbors: 
            
            # If the neighbor is already in the closed set, skip it
            
            tentative_gScore = current.g + 1
            
            # Check if the neighbor is in the open set
            idx = makeHash(neighbor)
            if idx in openSet2:
                node = openSet2[idx]
                if tentative_gScore < node.g:
                    del openSet2[idx]
                    #openSet.remove(node)
                    node.duplicate = True
                    neighbor.parent = current
                    neighbor.g = tentative_gScore
                    neighbor.f = neighbor.g + heuristic(neighbor)
                    #heapq.heapify(openSet)
                    heapq.heappush(openSet, neighbor)
                    openSet2[idx] = neighbor


            else:
                neighbor.parent = current
                neighbor.g = tentative_gScore
                neighbor.f = neighbor.g + heuristic(neighbor)
                heapq.heappush(openSet, neighbor)
                openSet2[idx] = neighbor
                    
            
# Compare two board states
def listCompare(list1, list2):
    for i in range(0,7):
        for j in range(0,7):
            if type(list1[i][j]) is not type(list2[i][j]):
                return False

    return True

# Calculates the heuristic for a given board state
"""
def heuristic(node):
    h = 0
    for piece in node.pieces:
        min_dist = 10
        for tile in exit_tiles[piece.colour]:
            if node.distance(piece.pos, tile) <= min_dist:
                min_dist = node.distance(piece.pos, tile)
        h += (min_dist / 2) + 1
    return h
"""

# Calculates the heuristic for a given board state
def heuristic(node):
    h = 0
    for piece in node.pieces:
        
        h += (min_dist(node, piece)-position_value(node, piece) / 2) + 1
    return h

def position_value(node, piece):
    adjacent_piece = pickle.loads(pickle.dumps(piece))
    value = 0
    if piece.pos in exit_tiles[piece.colour]:
        return value
    for move in node.moves:
        adjacent_piece.pos = (piece.pos[0] + move[0], piece.pos[1] + move[1])
        if checkJump(node, piece.pos, move) and on_board(adjacent_piece.pos):

            adjacent_min = min_dist(node, adjacent_piece)
            current_min = min_dist(node, piece)
            if adjacent_min < current_min:
                value += 0.6
            if adjacent_min == current_min:
                value += 0.35

            if adjacent_min == current_min+1:
                value += 0.15
    return value
        

def min_dist(node, piece):
    min_dist = 10
    if len(node.pieces) > 1:
        newBoard = pickle.loads(pickle.dumps(node))
        newPiece = pickle.loads(pickle.dumps(piece))
        for piece2 in newBoard.pieces:
            newBoard.tiles[piece2.pos[1]+3][piece2.pos[0]+3] = None
        newBoard.pieces = [newPiece]
        newBoard.tiles[newPiece.pos[1]+3][newPiece.pos[0]+3] = newPiece
        path = A_Star(newBoard)
        return len(path)
    else:
        for tile in exit_tiles[piece.colour]:
            if board.distance(piece.pos, tile) <= min_dist:
                min_dist = board.distance(piece.pos, tile)
    
    return min_dist

def on_board(position):
    if position[0] >= -3 and position[0] <= 3 and position[1] >= -3 and position[1] <= 3:
        return True
    else:
        return False

def checkJump(node, position, move):
    new = (position[0] + move[0], position[1] + move[1])
    new_jump = (position[0] + 2*move[0], position[1] + 2*move[1])
    if on_board(new) and on_board(new_jump):
        if type(board.tiles[new[0]+3][new[1]+3]) == Block.Block or type(board.tiles[new[0]+3][new[1]+3]) == Piece.Piece and type(board.tiles[new_jump[0]+3][new_jump[1]+3]) == None:
            return True
        else:
            return False

# Check if the goal has been reached
def checkGoal(state):
    if not state.pieces:
        return True
    else:
        return False

# Retrace the path from the goal state to the initial state
def retrace(goal):
    path = [goal]
    cur = goal
    while cur.parent is not None:
        cur = cur.parent
        path.insert(0,cur)
        #cur = cur.parent
    # path.insert(0,cur)
    return path

# Find the neighbor states of a given board state
def getNeighbors(current, closedSet):
    neighbors = []
    moves = current.getMoves()
    
    # getMoves returns only a Piece object if a piece is on an exit tile
    # In that case, the only neighbor is the same board with that piece removed
    if isinstance(moves, Piece.Piece):
        #neighbor = copy.deepcopy(current) 
        neighbor = pickle.loads(pickle.dumps(current))
        neighbor.toMove = None
        neighbor.toMove = ['e'] + list(moves.pos)

        for piece in neighbor.pieces:
            if piece.pos == moves.pos:
                neighbor.pieces.remove(piece)
                break

        neighbor.tiles[moves.pos[1]+3][moves.pos[0]+3] = None
        
        neighbors.append(neighbor)
        return neighbors
    
    # Otherwise, it returns a list of possible moves
    # In that case, return a list of board states with those moves applied
    for move in moves:
        current.move(move[1], move[2])
        idx = makeHash(current)
        """
        idx = str(len(current.pieces)) + ":"
        for p in current.pieces:
            idx += str(p.pos[0]) + ":"
            idx += str(p.pos[1])
            """

        if idx not in closedSet:
            neighbor = pickle.loads(pickle.dumps(current))
            #neighbor = copy.deepcopy(current)
            neighbor.toMove = None
            neighbor.toMove = list(move) 
            neighbors.append(neighbor)

        current.move(move[2], move[1]) 

    return neighbors

# Functions for printing board state
def make_bdict(state):

    bdict = {}

    for row in state.tiles:
        for col in row:
            if isinstance(col, Piece.Piece):
                bdict[col.pos] = 'p'
            elif isinstance(col, Block.Block):
                bdict[col.pos] = 'b'

    return bdict

def print_board(board_dict, message="", debug=False, **kwargs):
    """
    Helper function to print a drawing of a hexagonal board's contents.
    
    Arguments:

    * `board_dict` -- dictionary with tuples for keys and anything printable
    for values. The tuple keys are interpreted as hexagonal coordinates (using 
    the axial coordinate system outlined in the project specification) and the 
    values are formatted as strings and placed in the drawing at the corres- 
    ponding location (only the first 5 characters of each string are used, to 
    keep the drawings small). Coordinates with missing values are left blank.

    Keyword arguments:

    * `message` -- an optional message to include on the first line of the 
    drawing (above the board) -- default `""` (resulting in a blank message).
    * `debug` -- for a larger board drawing that includes the coordinates 
    inside each hex, set this to `True` -- default `False`.
    * Or, any other keyword arguments! They will be forwarded to `print()`.
    """

    # Set up the board template:
    if not debug:
        # Use the normal board template (smaller, not showing coordinates)
        template = """# {0}
#           .-'-._.-'-._.-'-._.-'-.
#          |{16:}|{23:}|{29:}|{34:}| 
#        .-'-._.-'-._.-'-._.-'-._.-'-.
#       |{10:}|{17:}|{24:}|{30:}|{35:}| 
#     .-'-._.-'-._.-'-._.-'-._.-'-._.-'-.
#    |{05:}|{11:}|{18:}|{25:}|{31:}|{36:}| 
#  .-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-.
# |{01:}|{06:}|{12:}|{19:}|{26:}|{32:}|{37:}| 
# '-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'
#    |{02:}|{07:}|{13:}|{20:}|{27:}|{33:}| 
#    '-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'
#       |{03:}|{08:}|{14:}|{21:}|{28:}| 
#       '-._.-'-._.-'-._.-'-._.-'-._.-'
#          |{04:}|{09:}|{15:}|{22:}|
#          '-._.-'-._.-'-._.-'-._.-'"""
    else:
        # Use the debug board template (larger, showing coordinates)
        template = """# {0}
#              ,-' `-._,-' `-._,-' `-._,-' `-.
#             | {16:} | {23:} | {29:} | {34:} | 
#             |  0,-3 |  1,-3 |  2,-3 |  3,-3 |
#          ,-' `-._,-' `-._,-' `-._,-' `-._,-' `-.
#         | {10:} | {17:} | {24:} | {30:} | {35:} |
#         | -1,-2 |  0,-2 |  1,-2 |  2,-2 |  3,-2 |
#      ,-' `-._,-' `-._,-' `-._,-' `-._,-' `-._,-' `-. 
#     | {05:} | {11:} | {18:} | {25:} | {31:} | {36:} |
#     | -2,-1 | -1,-1 |  0,-1 |  1,-1 |  2,-1 |  3,-1 |
#  ,-' `-._,-' `-._,-' `-._,-' `-._,-' `-._,-' `-._,-' `-.
# | {01:} | {06:} | {12:} | {19:} | {26:} | {32:} | {37:} |
# | -3, 0 | -2, 0 | -1, 0 |  0, 0 |  1, 0 |  2, 0 |  3, 0 |
#  `-._,-' `-._,-' `-._,-' `-._,-' `-._,-' `-._,-' `-._,-' 
#     | {02:} | {07:} | {13:} | {20:} | {27:} | {33:} |
#     | -3, 1 | -2, 1 | -1, 1 |  0, 1 |  1, 1 |  2, 1 |
#      `-._,-' `-._,-' `-._,-' `-._,-' `-._,-' `-._,-' 
#         | {03:} | {08:} | {14:} | {21:} | {28:} |
#         | -3, 2 | -2, 2 | -1, 2 |  0, 2 |  1, 2 | key:
#          `-._,-' `-._,-' `-._,-' `-._,-' `-._,-' ,-' `-.
#             | {04:} | {09:} | {15:} | {22:} |   | input |
#             | -3, 3 | -2, 3 | -1, 3 |  0, 3 |   |  q, r |
#              `-._,-' `-._,-' `-._,-' `-._,-'     `-._,-'"""

    # prepare the provided board contents as strings, formatted to size.
    ran = range(-3, +3+1)
    cells = []
    for qr in [(q,r) for q in ran for r in ran if -q-r in ran]:
        if qr in board_dict:
            cell = str(board_dict[qr]).center(5)
        else:
            cell = "     " # 5 spaces will fill a cell
        cells.append(cell)

    # fill in the template to create the board drawing, then print!
    board = template.format(message, *cells)
    print(board, **kwargs)

if __name__ == '__main__':
    pr = cProfile.Profile()
    pr.enable()
    main()
    pr.disable()
    pr.print_stats(sort="time")

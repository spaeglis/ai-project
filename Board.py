class Board:
    # List of move directions
    moves = [(-1,0), (0,-1), (1,-1), (1,0), (0,1), (-1,1)]
    
    def __init__(self):
        # Marks whether the state is a duplicate within the open set
        self.duplicate = False

        # List of exit tiles for each colour
        self.exit_tiles = {'red':[(3,-3), (3,-2), (3,-1), (3,0)],
            'green':[(-3,3),(-2,3),(-1,3),(0,3)],
            'blue':[(0,-3),(-1,-2),(-2,-1),(-3,0)]}
        
        # Create board state array
        # None means an empty tile and 0 means a tile not on the board
        self.tiles = [[None for i in range(7)] for j in range(7)]
        self.tiles[0][0] = 0
        self.tiles[0][1] = 0
        self.tiles[0][2] = 0
        self.tiles[1][0] = 0
        self.tiles[1][1] = 0
        self.tiles[2][0] = 0
        self.tiles[4][6] = 0
        self.tiles[5][5] = 0
        self.tiles[5][6] = 0
        self.tiles[6][4] = 0
        self.tiles[6][5] = 0
        self.tiles[6][6] = 0
        
        # List of pieces on the board
        self.pieces = []
        
        # g and f values for a star
        self.g = 0
        self.f = 0

        # The move applied to reach this board state
        self.toMove = []

        # The previous board state
        self.parent = None
    
    def __lt__(self, other):
        return (self.f < other.f)

    # Returns a list of valid moves for this board state in the form: (move_type, (pos_from), (pos_to))
    # move_type is either m, j or e for move jump and exit
    def getMoves(self):
        valid_moves = []
        for piece in self.pieces:

            # If the piece is on an exit tile, return that piece
            if piece.pos in self.exit_tiles[piece.colour]:
                return piece
            p_r = piece.pos[1]+3
            p_q = piece.pos[0]+3

            # Otherwise, try all moves and return valid ones
            for move in self.moves:
                if (
                        p_r + move[1] > 6 or 
                        p_r + move[1] < 0 or 
                        p_q + move[0] > 6 or 
                        p_q + move[0] < 0
                    ):
                    continue

                if self.tiles[p_r + move[1]][p_q + move[0]] is None:
                    valid_moves.append(('m',(p_q-3, p_r-3),(p_q-3 + move[0], p_r-3 + move[1])))

                elif (
                        p_r + move[1]*2 <= 6 and 
                        p_r + move[1]*2 >= 0 and 
                        p_q + move[0]*2 <= 6 and 
                        p_q + move[0]*2 >= 0
                    ):
                    if self.tiles[p_r + move[1]*2][p_q + move[0]*2] is None:
                        valid_moves.append(('j',(p_q-3, p_r-3),(p_q-3 + move[0]*2, p_r-3 + move[1]*2)))
        
        return valid_moves
    
    # Moves a pice from one position to another
    def move(self, t_from, t_to):
        piece = self.tiles[t_from[1]+3][t_from[0]+3]
        self.tiles[t_from[1]+3][t_from[0]+3] = None
        self.tiles[t_to[1]+3][t_to[0]+3] = piece
        self.tiles[t_to[1]+3][t_to[0]+3].pos = t_to
    
    # Calculate the distance from one position to another
    def distance(self, start, end):
        q1 = start[0]
        q2 = end[0]
        r1 = start[1]
        r2 = end[1]
        return (abs(q1 - q2) + abs(q1 + r1 - q2 - r2) + abs(r1 - r2))/2

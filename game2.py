import pickle
import sys
import heapq
import json
import copy
import Board
import Piece
import Block

exit_tiles = {'red':[(3,-3),(3,-2),(3,-1),(3,0)],
        'green':[(-3,3),(-2,3),(-1,3),(0,3)],
        'blue':[(0,-3),(-1,-2),(-2,-1),(-3,0)]}

# Initialize board
board = Board.Board()

def main():
    with open(sys.argv[1]) as file:
        
        # Load initial board state
        data = json.load(file)
     
        # Add pieces and blocks to the board
        for p in data['pieces']:
            piece = Piece.Piece(tuple(p), data['colour'])
            board.tiles[piece.pos[1]+3][piece.pos[0]+3] = piece
            board.pieces.append(piece)
        
        for b in data['blocks']:
            block = Block.Block(tuple(b))
            board.tiles[block.pos[1]+3][block.pos[0]+3] = block

        colour = data['colour']
        

        # Remove from exit tiles list any tile with a block on it
        # because a piece can't exit from them
        nonBlockTiles = []
        for tile in exit_tiles[colour]:
            if type(board.tiles[tile[1]+3][tile[0]+3]) == Block.Block:
                pass
            else:
                nonBlockTiles.append(tile)
        exit_tiles[colour] = nonBlockTiles
        board.exit_tiles[colour] = nonBlockTiles 

        # Run a* search and print solution if one exists
        path = A_Star(board)
        
        if path is None:
            print("No path found")
        
        else:
            for node in path:
                if not node.toMove:
                    continue
                if node.toMove[0] == 'e':
                    print("EXIT from (" + str(node.toMove[1]) + ", " + str(node.toMove[2]) + ").")

                elif node.toMove[0] == 'm':
                    print("MOVE from " + str(node.toMove[1]) + " to " + str(node.toMove[2]) + ".")

                elif node.toMove[0] == 'j':
                    print ("JUMP from " + str(node.toMove[1]) + " to " + str(node.toMove[2]) + ".")



def A_Star(start):

    # Initialise open and closed sets
    closedSet = {}
    openSet = [start]
    openSet2 = {}
    idx = makeHash(start)
    openSet2[idx] = start
    
    # Initial path length and heuristic
    start.g = 0

    start.f = heuristic(start)
    
    while openSet:

        # Find node in open set with lowest f value and set it as the "current" node
        # ignoring duplicate nodes
        current = heapq.heappop(openSet)
        while current.duplicate == True:
            current = heapq.heappop(openSet)
        
        # If found node is the goal, retrace the path, and return the solution
        if checkGoal(current):
            return retrace(current) 

        # Remove the current node from the open set and add it to the closed set
        idx = makeHash(current)
        del openSet2[idx]
        closedSet[idx] = current
        
        # Find all neighbors of the current node, excluding those already in 
        # the closed set
        neighbors = getNeighbors(current, closedSet)
        
        # Iterate through the neighbors of the current node
        for neighbor in neighbors: 
 
            tentative_gScore = current.g + 1
            
            # Check if the neighbor is in the open set
            # If it is, and the new neighbor g score is less than the existing
            # node's score, mark the existing node as a duplicate and insert
            # the new neighbor
            idx = makeHash(neighbor)
            if idx in openSet2:
                node = openSet2[idx]
                if tentative_gScore < node.g:
                    del openSet2[idx]
                    node.duplicate = True
                    neighbor.parent = current
                    neighbor.g = tentative_gScore
                    neighbor.f = neighbor.g + heuristic(neighbor)
                    heapq.heappush(openSet, neighbor)
                    openSet2[idx] = neighbor

            # If the neighbor is not in the open set, add it
            else:
                neighbor.parent = current
                neighbor.g = tentative_gScore
                neighbor.f = neighbor.g + heuristic(neighbor)
                heapq.heappush(openSet, neighbor)
                openSet2[idx] = neighbor

# Generate a unique string for a given board state
# for use in the closed and open sets
# in the format: no. pieces on board then q and r
# coordinates for each piece
def makeHash(node):
    h = str(len(node.pieces))
    spos = []
    for p in node.pieces:
        s = str(p.pos[0]) + str(p.pos[1])
        spos.append(s)
    spos.sort()
    for piece in spos:
        h += piece
    return h

# Calculates the heuristic for a given board state
def heuristic(node):
    h = 0
    for piece in node.pieces:
        
        h += (min_dist(piece)-position_value(node, piece) / 2) + 1
    return h

# Calculates a "score" for a position based on the number of available jumps
# that get you closer to the goal state
def position_value(node, piece):
    adjacent_piece = pickle.loads(pickle.dumps(piece))
    value = 0
    if piece.pos in exit_tiles[piece.colour]:
        return value
    for move in node.moves:
        adjacent_piece.pos = (piece.pos[0] + move[0], piece.pos[1] + move[1])
        adjacent_min = min_dist(adjacent_piece)
        current_min = min_dist(piece)
        if checkJump(node, piece.pos, move) and on_board(adjacent_piece.pos):
            if adjacent_min < current_min:
                value += 0.5
            if adjacent_min == current_min:
                value += 0.25

            if adjacent_min == current_min+1:
                value += 0.15
        if (
                on_board(adjacent_piece.pos) and
                type(node.tiles[adjacent_piece.pos[1]+3][adjacent_piece.pos[0]+3]) == Piece.Piece
            ):
            value += 0.1
    return value
        
# Calculates the distance of a piece to the nearest exit tile
def min_dist(piece):
    min_dist = 10
    for tile in exit_tiles[piece.colour]:
        if board.distance(piece.pos, tile) <= min_dist:
            min_dist = board.distance(piece.pos, tile)
    return min_dist

# Check if a position is on the board
def on_board(position):
    if position[0] >= -3 and position[0] <= 3 and position[1] >= -3 and position[1] <= 3:
        return True
    else:
        return False

# Check if a jump is possible from a position in a given direction
def checkJump(node, position, move):
    new = (position[0] + move[0], position[1] + move[1])
    new_jump = (position[0] + 2*move[0], position[1] + 2*move[1])
    if on_board(new) and on_board(new_jump):
        if (
                type(board.tiles[new[0]+3][new[1]+3]) == Block.Block or 
                type(board.tiles[new[0]+3][new[1]+3]) == Piece.Piece and 
                type(board.tiles[new_jump[0]+3][new_jump[1]+3]) == None
            ):
            return True
        else:
            return False

# Check if the goal has been reached
def checkGoal(state):
    if not state.pieces:
        return True
    else:
        return False

# Retrace the path from the goal state to the initial state
def retrace(goal):
    path = [goal]
    cur = goal
    while cur.parent is not None:
        cur = cur.parent
        path.insert(0,cur)
    return path

# Find the neighbor states of a given board state
def getNeighbors(current, closedSet):
    neighbors = []
    moves = current.getMoves()
    
    # getMoves returns only a Piece object if a piece is on an exit tile
    # In that case, the only neighbor is the same board with that piece removed
    # which is an exit move
    if isinstance(moves, Piece.Piece):
        neighbor = pickle.loads(pickle.dumps(current))
        neighbor.toMove = None
        neighbor.toMove = ['e'] + list(moves.pos)

        for piece in neighbor.pieces:
            if piece.pos == moves.pos:
                neighbor.pieces.remove(piece)
                break

        neighbor.tiles[moves.pos[1]+3][moves.pos[0]+3] = None
        
        neighbors.append(neighbor)
        return neighbors
    
    # Otherwise, it returns a list of possible moves
    # In that case, return a list of board states with those moves applied
    # excluding states found in the closed set
    for move in moves:
        current.move(move[1], move[2])
        idx = makeHash(current)

        if idx not in closedSet:
            neighbor = pickle.loads(pickle.dumps(current))
            neighbor.toMove = None
            neighbor.toMove = list(move) 
            if current.toMove == neighbor.toMove:
                print(current.toMove)
            neighbors.append(neighbor)

        current.move(move[2], move[1]) 

    return neighbors

if __name__ == '__main__':
    main()
